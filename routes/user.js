var controllers = require('../controllers');
const Joi = require('joi');

module.exports = [
  {
    method: 'GET',
    path: '/articles',
    config: {
      auth: false,
    },
    handler: controllers.users.articles
  }
];
