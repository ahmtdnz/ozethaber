var Hapi = require('hapi');

var path = require('path');
var settings = require('config');
var user = require('./routes/user');
var plugins = require('./plugins');
var models = require('./models');

var server = new Hapi.Server({
  connections:{
    routes:{
      cors:settings.cors
    }
  }
});
server.connection({
  port: process.env.PORT || settings.port,
  routes: { log: true }
});

// Export the server to be required elsewhere.
module.exports = server;

var initDb = function(cb){
  var sequelize = models.sequelize;

  //Test if we're in a sqlite memory database. we may need to run migrations.
  if(sequelize.getDialect()==='sqlite' &&
      (!sequelize.options.storage || sequelize.options.storage === ':memory:')){
    sequelize.getMigrator({
      path: process.cwd() + '/migrations',
    }).migrate().success(function(){
      // The migrations have been executed!
      cb();
    });
  } else {
    cb();
  }
};

var customVerifyFunc = function (decoded, request, callback) {
  return callback(null, true, decoded);
};
var setup = function(done){

	//Register all plugins
	server.register(plugins, function (err) {
		if (err) {
			throw err; // something bad happened loading a plugin
		}
    var privateKey = 'NeverShareYourSecret';
    server.auth.strategy('jwt', 'jwt',{
        key: privateKey,
        validateFunc: customVerifyFunc,
        verifyOptions: { algorithms: [ 'HS256' ] }
    });
    server.auth.default('jwt');
	});

  // Add the server routes
  server.route(user);

  initDb(function(){
    done();
  });
};

var start = function(){
  server.start(function(){
    server.log('info', 'Server running at: ' + server.info.uri);
  });
};

// If someone runs: "node server.js" then automatically start the server
if (path.basename(process.argv[1],'.js') == path.basename(__filename,'.js')) {
  setup(function(){
    start();
  });
}
