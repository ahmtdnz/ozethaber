const Good = require('good');
const JWT = require('hapi-auth-jwt2');

const plugins = [
    {
        register: Good,
        options: {
            reporters: [{
                reporter: require('good-console'),
                events: {
                    response: '*',
                    log: '*'
                }
            }]
        }
    },
    {
        register: JWT
    }
];

module.exports = plugins;
