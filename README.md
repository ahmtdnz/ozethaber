# Golazzo

## Directory structure ##

The structure of the boilerplate and explanation follows:
```bash
hapi-boilerplate
├── app.js                  # Application entry point run with "node app"
├── config
│   ├── default.json        # common config for all environments
│   ├── development.json    # config for development environments
│   ├── production.json     # config for production environments
├── controllers
│   ├── index.js            # file that requires all controllers into a hash
│   └── users.js            # an example controller. use it for inspiration.
├── migrations              # migrations directory with an example migration. generated with "sequelize-cli"
│   └── 20141021121205-create-user.js
├── models
│   ├── index.js            # generated with "sequelize init". requires all models.
│   └── user.js             # an example model. generated with "sequelize-cli model:create"
├── package.json
├── plugins
│   └── index.js            # register plugins. add your custom plugins in this folder as well.
├── README.md
└── routes.js               # define all the routes in this file.
```

##Usage

Just clone the repository:

```bash
$ git clone https://github.com/vedatmahir/golazzo.git
```
run

```bash
$ npm install
```

and start coding.

To run the application run `node app`.
