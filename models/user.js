"use strict";

module.exports = function(sequelize, DataTypes) {
  var users = sequelize.define("users", {
    email: {unique: true, type: DataTypes.STRING},
    password: DataTypes.STRING,
    username: {unique: true, type: DataTypes.STRING},
    isAdmin: DataTypes.BOOLEAN,
    avatar : DataTypes.STRING,
    position : DataTypes.STRING,
    speed : DataTypes.INTEGER,
    power : DataTypes.INTEGER,
    goal : DataTypes.INTEGER,
    shoot : DataTypes.INTEGER
  });

  users.sync().then(function () {

  });
  return users;
};
