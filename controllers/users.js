var models = require('../models');
const jwt = require('jsonwebtoken');
const axios = require('axios');
let hurriyetsdk = require('hurriyet-js-sdk');
let HurriyetAPI = new hurriyetsdk({ token: 'a31e4ff560e0437f9ca52a22ec32d673',fetch: require('node-fetch')});
let htmlToText = require('html-to-text');
let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtYWlsIjoiYWhtZXRAaG91c2VvZmFwcHMuY29tIiwiaWF0IjoxNDgyNjE4MzU1LCJleHAiOjE0ODI2MjE5NTV9.yyL66QP2bqS7b_50ECWTrX_CG2CarnVD-jZNhlkNbjA';


module.exports = {

	articles:function (request, reply) {
		HurriyetAPI.getArticles({articleId : 40316642}).then((response) => {
      var text = htmlToText.fromString(response.Text, {
          wordwrap: 130
      });

      return axios({
    	  method: 'post',
    	  url: `http://ozetle.htkibar.com/summarize`,
    		headers: {
    		},
    	  data: {
    		 	text: text,
     	    clusterCount: 25,
     	    token : token
    		 }
    	}).then(function(response){
        reply({
          data: response
        })
      });

		});
	}

};
